rpolyapost <- function(M, y, N) {
   n <- length(y)
   out <- matrix(0, nrow=M, ncol=N)
   for(m in 1:M) {
     out[m, 1:n] <- y
     for(j in (n+1):N) {
       I <- sample(1:(j-1), 1)
       out[m, j] <- out[m, I]
     }
   }
   return(out)
}

y <- c(20.89, 19.01, 6.88, 1.61, 1.34, 4.57, 9.13, 8.83, 29.98, 1.55)
set.seed(7)
yy <- rpolyapost(5000, y, 50)
avg <- apply(yy, 1, mean)
hist(avg, freq=F, xlab="", col="gray", border="white", main="")
points(mean(y), 0, pch="X")
abline(v=mean(avg))
