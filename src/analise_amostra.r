load("AmostraVirtual.Rda")
dados <- amostra

# "abaixo_media_semanal": Verifica a quantidade de saidas nas quais 
# foram capturados menos que a media semanal (tem-se dados de 20 saidas)
media_saida_semanal <- rep(0, 4)
dias_sem_saida <- rep(0, 4)
media_semanal <- rep(0, 4)
abaixo_media_semanal <- rep(0, 4)
dias_zeros <- rep(0, 4)
prop_zeros <- rep(0, 4)

for( i in 1:4 ){
  media_saida_semanal[i] <- sum(dados[, i], na.rm=TRUE) / 15
  dias_sem_saida[i] <- sum(dados[, i] == 0, na.rm=TRUE)
  media_semanal[i] <- sum(dados[, i+4], na.rm = TRUE) / 15
  abaixo_media_semanal[i] <- sum(dados[, i+4] < media_semanal[i], na.rm = TRUE)
  dias_zeros[i] <- sum(dados[, i+4] == 0, na.rm=TRUE)
  prop_zeros[i] <- (dias_zeros[i]/20)*100
}



ams.amp <- rep(0, 4)
ams.media <- rep(0, 4)
ams.variacao <- rep(0, 4)
ams.dpadrao  <- rep(0, 4)
ams.coevar   <- rep(0, 4)

ams.u1.qt <- quantile(dados[, 5], na.rm = T)
ams.u2.qt <- quantile(dados[, 6], na.rm = T)
ams.u3.qt <- quantile(dados[, 7], na.rm = T)
ams.u4.qt <- quantile(dados[, 8], na.rm = T)

for( i in 1:4 ) {
  ams.amp[i]      <- diff(range(dados[, i+4], na.rm = T))
  ams.media[i]    <- mean(dados[, i+4], na.rm = T)
  ams.variacao[i] <- var(dados[, i+4], na.rm = T)
  ams.dpadrao[i]  <- sd(dados[, i+4], na.rm = T)
  ams.coevar[i]   <- 100*(ams.dpadrao[i]/ams.media[i])
  
  #hist(dados[, i+4])
}





#############
#
# Censo
#
#############
load("CensoVirtual.Rda")
dados <- censo

c.media_saida_semanal <- rep(0, 4)
c.dias_sem_saida <- rep(0, 4)
c.dias_zeros <- rep(0, 4)
c.prop_zeros <- rep(0, 4)
c.ams.amp <- rep(0, 4)
c.ams.media <- rep(0, 4)
c.ams.variacao <- rep(0, 4)
c.ams.dpadrao  <- rep(0, 4)
c.ams.coevar   <- rep(0, 4)

# Total de captura na semana i
c.ui.total = matrix( rep(0, 345*4), ncol=4 )

for( i in 1:4 ){
  c.media_saida_semanal[i] <- mean(dados[, i], na.rm=TRUE)
  c.dias_sem_saida[i] <- sum(dados[, i] == 0, na.rm=TRUE)
  
  c.ui.total[,i] = dados[,i+4] + dados[,i+8] + dados[,i+12] + dados[,i+16]
  c.dias_zeros[i] <- sum(c.ui.total[,i] == 0, na.rm=TRUE)
  c.prop_zeros[i] <- (c.dias_zeros[i]/345)*100
  
  
  c.ams.amp[i]      <- diff(range(c.ui.total[, i], na.rm = T))
  c.ams.media[i]    <- mean(c.ui.total[, i], na.rm = T)
  c.ams.variacao[i] <- var(c.ui.total[, i], na.rm = T)
  c.ams.dpadrao[i]  <- sd(c.ui.total[, i], na.rm = T)
  c.ams.coevar[i]   <- 100*(c.ams.dpadrao[i]/c.ams.media[i])
}

c.ams.u1.qt <- quantile(c.ui.total[, 1], na.rm = T)
c.ams.u2.qt <- quantile(c.ui.total[, 2], na.rm = T)
c.ams.u3.qt <- quantile(c.ui.total[, 3], na.rm = T)
c.ams.u4.qt <- quantile(c.ui.total[, 4], na.rm = T)
