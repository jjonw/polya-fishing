#######################
# Geracao de Dados para construir uma populacao censitaria de referencia
# [E extrair uma amostra] 
######################

rm(list=ls())

# Necessario para reproducibilidade
set.seed(379)

# definir todos os parametros
# 1. probabilidades para inclusao de cada tipo 'i' de pescado
p1 <- 0.3
p21 <- 0.9; pp21 <- 0.15
p43 <- 0.5; pp43 <- 0.8
# 2. abundancia esperada de cada tipo 'i' de pescado por viagem
a <- c(40,5,100,15)
# 3. numero esperado de viagens por semana 'j'
mu <- c(4, 1/2, 1.5, 2.5)
# 3. total de pescadores o embarcacoes cadastrados
N <- 345

# geracao do numero de viagens por semana 'j'  (fj)
f1 <- rpois(N,mu[1])
f2 <- rpois(N,mu[2])
f3 <- rpois(N,mu[3])
f4 <- rpois(N,mu[4])
V <- cbind(f1,f2,f3,f4) 

# geracao das inclusoes de pescado 'i' em cada semana 'j' (Xj)
# semana j = 1
x1 <- rbinom(N,1,p1)
p2 <- x1*p21 + (1-x1)*pp21
x2 <- rbinom(N,1,p2)
x3 <- (1-x1)*(1-x2)
p4 <- x3*p43 + (1-x3)*pp43
x4 <- rbinom(N,1,p4)
X1 <- cbind(x1,x2,x3,x4)
# semana j = 2
x1 <- rbinom(N,1,p1)
p2 <- x1*p21 + (1-x1)*pp21
x2 <- rbinom(N,1,p2)
x3 <- (1-x1)*(1-x2)
p4 <- x3*p43 + (1-x3)*pp43
x4 <- rbinom(N,1,p4)
X2 <- cbind(x1,x2,x3,x4)
# semana j = 3
x1 <- rbinom(N,1,p1)
p2 <- x1*p21 + (1-x1)*pp21
x2 <- rbinom(N,1,p2)
x3 <- (1-x1)*(1-x2)
p4 <- x3*p43 + (1-x3)*pp43
x4 <- rbinom(N,1,p4)
X3 <- cbind(x1,x2,x3,x4)
# semana j = 4
x1 <- rbinom(N,1,p1)
p2 <- x1*p21 + (1-x1)*pp21
x2 <- rbinom(N,1,p2)
x3 <- (1-x1)*(1-x2)
p4 <- x3*p43 + (1-x3)*pp43
x4 <- rbinom(N,1,p4)
X4 <- cbind(x1,x2,x3,x4)

# geracao do volume de pescado 'i' em cada semana 'j' (Yj)
# semana j = 1  
Y1 <- matrix( rep(NA, N*4), ncol=4 )

for(k in 1:N){
  for(i in 1:4){
  	if(V[k,1]==0)  
  		Y1[k,i] <- 0
  	else
 		Y1[k,i] <- round( X1[k,i]*sum( rchisq(V[k, 1], a[i]/2) ), 0 )  
  }
}
# semana j = 2  
Y2 <- matrix(rep(NA,N*4),ncol=4)
for(k in 1:N){
  for(i in 1:4){
    if(V[k,2]==0)  Y2[k,i] <- 0
    else
    Y2[k,i] <- round(X2[k,i]*sum(rchisq(V[k,2],a[i]/2)),0)  
  }
}
# semana j = 3  
Y3 <- matrix(rep(NA,N*4),ncol=4)
for(k in 1:N){
  for(i in 1:4){
    if(V[k,3]==0)  Y3[k,i] <- 0
    else
    Y3[k,i] <- round(X3[k,i]*sum(rchisq(V[k,3],a[i]/2)),0)  
  }
}
# semana j = 4  
Y4 <- matrix(rep(NA,N*4),ncol=4)
for(k in 1:N){
  for(i in 1:4){
    if(V[k,4]==0)  Y4[k,i] <- 0
    else
    Y4[k,i] <- round(X4[k,i]*sum(rchisq(V[k,4],a[i]/2)),0)  
  }
}

# construcao da matriz completa de esforco e captura
nomes <- c("f.1","f.2","f.3","f.4",
           "u1.1","u2.1","u3.1","u4.1",
           "u1.2","u2.2","u3.2","u4.2",
           "u1.3","u2.3","u3.3","u4.3",
           "u1.4","u2.4","u3.4","u4.4")
censo <- data.frame(V,Y1,Y2,Y3,Y4)
colnames(censo) <- nomes
str(censo)
head(censo)

#load(file="CensoVirtual.Rda")
#sumarios censitarios
totais <- apply(censo,2,sum)
EsfTotal <- sum(totais[1:4])
U1Total <- sum(totais[c(5,9,13,17)])
U2Total <- sum(totais[c(6,10,14,18)])
U3Total <- sum(totais[c(7,11,15,19)])
U4Total <- sum(totais[c(8,12,16,20)])
UGTotal <- sum(U1Total+U2Total+U3Total+U4Total)
c(EsfTotal=EsfTotal,
  U1Total=U1Total,
  U2Total=U2Total, 
  U3Total=U3Total,
  U4Total=U4Total,
  UGTotal=UGTotal)

# enviar a populacao censitaria para arquivo
save(censo,file="CensoVirtual.Rda")

# criar a amostra obtida por levantamento amostral
nf <- 15
nu <- 5
n <- nf*4
 

amostra <- data.frame(
  f1 = c(censo$f.1[1:nf],rep(NA,n-nf)),
  f2 = c(rep(NA,nf),censo$f.2[(nf+1):(2*nf)],rep(NA,n-2*nf)),
  f3 = c(rep(NA,2*nf),censo$f.3[(2*nf+1):(3*nf)],rep(NA,n-3*nf)),
  f4= c(rep(NA,3*nf),censo$f.4[(3*nf+1):(4*nf)]),
  U1 = c(censo$u1.1[1],rep(NA,2),
         censo$u1.1[4],rep(NA,2),
         censo$u1.1[7],rep(NA,2),
         censo$u1.1[10],rep(NA,2),
         censo$u1.1[13],rep(NA,2),
         censo$u1.2[16],rep(NA,2),
         censo$u1.2[19],rep(NA,2),
         censo$u1.2[22],rep(NA,2),
         censo$u1.2[25],rep(NA,2),
         censo$u1.2[28],rep(NA,2),
         censo$u1.3[31],rep(NA,2),
         censo$u1.3[34],rep(NA,2),
         censo$u1.3[37],rep(NA,2),
         censo$u1.3[40],rep(NA,2),
         censo$u1.3[43],rep(NA,2),
         censo$u1.4[46],rep(NA,2),
         censo$u1.4[49],rep(NA,2),
         censo$u1.4[52],rep(NA,2),
         censo$u1.4[55],rep(NA,2),
         censo$u1.4[58],rep(NA,2)),
  U2 = c(censo$u2.1[1],rep(NA,2),
         censo$u2.1[4],rep(NA,2),
         censo$u2.1[7],rep(NA,2),
         censo$u2.1[10],rep(NA,2),
         censo$u2.1[13],rep(NA,2),
         censo$u2.2[16],rep(NA,2),
         censo$u2.2[19],rep(NA,2),
         censo$u2.2[22],rep(NA,2),
         censo$u2.2[25],rep(NA,2),
         censo$u2.2[28],rep(NA,2),
         censo$u2.3[31],rep(NA,2),
         censo$u2.3[34],rep(NA,2),
         censo$u2.3[37],rep(NA,2),
         censo$u2.3[40],rep(NA,2),
         censo$u2.3[43],rep(NA,2),
         censo$u2.4[46],rep(NA,2),
         censo$u2.4[49],rep(NA,2),
         censo$u2.4[52],rep(NA,2),
         censo$u2.4[55],rep(NA,2),
         censo$u2.4[58],rep(NA,2)),
  U3 = c(censo$u3.1[1],rep(NA,2),
         censo$u3.1[4],rep(NA,2),
         censo$u3.1[7],rep(NA,2),
         censo$u3.1[10],rep(NA,2),
         censo$u3.1[13],rep(NA,2),
         censo$u3.2[16],rep(NA,2),
         censo$u3.2[19],rep(NA,2),
         censo$u3.2[22],rep(NA,2),
         censo$u3.2[25],rep(NA,2),
         censo$u3.2[28],rep(NA,2),
         censo$u3.3[31],rep(NA,2),
         censo$u3.3[34],rep(NA,2),
         censo$u3.3[37],rep(NA,2),
         censo$u3.3[40],rep(NA,2),
         censo$u3.3[43],rep(NA,2),
         censo$u3.4[46],rep(NA,2),
         censo$u3.4[49],rep(NA,2),
         censo$u3.4[52],rep(NA,2),
         censo$u3.4[55],rep(NA,2),
         censo$u3.4[58],rep(NA,2)),
  U4 = c(censo$u4.1[1],rep(NA,2),
         censo$u4.1[4],rep(NA,2),
         censo$u4.1[7],rep(NA,2),
         censo$u4.1[10],rep(NA,2),
         censo$u4.1[13],rep(NA,2),
         censo$u4.2[16],rep(NA,2),
         censo$u4.2[19],rep(NA,2),
         censo$u4.2[22],rep(NA,2),
         censo$u4.2[25],rep(NA,2),
         censo$u4.2[28],rep(NA,2),
         censo$u4.3[31],rep(NA,2),
         censo$u4.3[34],rep(NA,2),
         censo$u4.3[37],rep(NA,2),
         censo$u4.3[40],rep(NA,2),
         censo$u4.3[43],rep(NA,2),
         censo$u4.4[46],rep(NA,2),
         censo$u4.4[49],rep(NA,2),
         censo$u4.4[52],rep(NA,2),
         censo$u4.4[55],rep(NA,2),
         censo$u4.4[58],rep(NA,2)))
amostra
#save(amostra,file="AmostraVirtual.Rda")