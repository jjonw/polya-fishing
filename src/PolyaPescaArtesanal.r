#------------------#
# Aplicacao da expansão de Polya para o estudo piloto da
# pesca artesanal de Rio Grande.
# Produzem-se posteriores de Polya para Producao Total e 
# também por categoria de pescado 
# (VERSAO COMPLETA PARA 4 SEMANAS)
#------------------#
#   PG Kinas 01 setembro 2018
#------------------#

# preliminares
rm(list=ls())
library(polyapost)
library(HDInterval)
#library(dplyr)

# carregar os dados e colocar uma cópia em 'dados'
load("AmostraVirtual.Rda")
dados <- amostra
str(dados)
names(dados)

# calcular a captura média por viagem para a amostra
for(i in 1:15){
  if(dados[i,1]==0) dados[i,5:8] <- dados[i,5:8] 
  else
    dados[i,5:8] <- dados[i,5:8]/dados[i,1]
}
for(i in 16:30){
  if(dados[i,2]==0) dados[i,5:8] <- dados[i,5:8] 
  else
  dados[i,5:8] <- dados[i,5:8]/dados[i,2]
}
for(i in 31:45){
  if(dados[i,3]==0) dados[i,5:8] <- dados[i,5:8] 
  else
    dados[i,5:8] <- dados[i,5:8]/dados[i,3]
}
for(i in 46:60){
  if(dados[i,4]==0) dados[i,5:8] <- dados[i,5:8] 
  else
    dados[i,5:8] <- dados[i,5:8]/dados[i,4]
}

# preparar a planilha para efetuar a amostragem de polya
# acrescentam-se linhas com NA até completar um tota de N linhas
# sendo N = total de pescadores (345)
N <- 345
n.s <- dim(dados)[1]
dadosPop <- data.frame(f1 = c(dados$f1,rep(NA,N-n.s)),
                       f2 = c(dados$f2,rep(NA,N-n.s)),
                       f3 = c(dados$f3,rep(NA,N-n.s)),
                       f4 = c(dados$f4,rep(NA,N-n.s)),
                       U1 = c(dados$U1,rep(NA,N-n.s)),
                       U2 = c(dados$U2,rep(NA,N-n.s)),
                       U3 = c(dados$U3,rep(NA,N-n.s)),
                       U4 = c(dados$U4,rep(NA,N-n.s)))
str(dadosPop)

####
# Efetuar a amostragem po urna de Polya
# (a). selecionar em cada variável os numeros das linhas em que ha 
# informacao (isto e, celulas sem NA)
# Nota: somente utilizo as linhas de U1, pois sera sempre observado
# o vetor (U1,U2,U3,U4). Nos casos em que nao ha descargas o valor é
# Zero para distingui-lo de NA
index <- 1:n.s
i.f1 <- index[!is.na(dados$f1)]
i.f2 <- index[!is.na(dados$f2)]
i.f3 <- index[!is.na(dados$f3)]
i.f4 <- index[!is.na(dados$f4)]
i.U1 <- index[!is.na(dados$U1)]

#(b.) definir o número de linhas a acrescentar ao arquivo com as
# informações dos 'n.s' pescadores entrevistados
# criação de espaco de memoria para utilizar no loop para guardar
# os desembarques completos de (U1,U2,U3,U4) (em Y) e para a
# soma UT = U1+U2+U3+U4 e esforco (Esf)
Ke <- N-n.s
Y <- matrix(rep(NA,N*4),ncol=4)
UT <- rep(NA,N)
Esf <- rep(NA,N)

#(c.) gerar a posterior de Polya com 'K' replicas do procedimento de 
# amostragem da urna de Polya, para as estimativas de producao total 
# UT (Ty.est) e de cada sub-grupo U1 (Ty1.est), U2 (Ty2.est)
# U3 (Ty3.est) e U4 (Ty3.est)
K <- 5000
Ty.est <- rep(NA,K) 
Ty1.est <- rep(NA,K)
Ty2.est <- rep(NA,K)
Ty3.est <- rep(NA,K)
Ty4.est <- rep(NA,K)
Tf.est <- rep(NA,K)

for(i in 1:K) {
  # simular da urna de Polya as linhas para cada variável
  po.f1 <- polyap(i.f1, Ke+(n.s-15))
  po.f2 <- polyap(i.f2, Ke+(n.s-15))
  po.f3 <- polyap(i.f3, Ke+(n.s-15))
  po.f4 <- polyap(i.f4, Ke+(n.s-15))
  po.U1 <- polyap(i.U1, Ke+(n.s-20))

  # associar as linhas simuladas com a sua posicao no arquivo completo
  # denotado 'dadosPop'
    ind.f1 <- c(po.f1)
    ind.f2 <- c(po.f2[16:30],po.f2[1:15],po.f2[31:N])
    ind.f3 <- c(po.f3[16:45],po.f3[1:15],po.f3[46:N])
    ind.f4 <- c(po.f4[16:60],po.f4[1:15],po.f4[61:N])
    ind.U1 <-  c(po.U1[1],po.U1[21:22],po.U1[2],po.U1[23:24],
                 po.U1[3],po.U1[25:26],po.U1[4],po.U1[27:28],
                 po.U1[5],po.U1[29:30],po.U1[6],po.U1[31:32],
                 po.U1[7],po.U1[33:34],po.U1[8],po.U1[35:36],
                 po.U1[9],po.U1[37:38],po.U1[10],po.U1[39:40],
                 po.U1[11],po.U1[41:42],po.U1[12],po.U1[43:44],
                 po.U1[13],po.U1[45:46],po.U1[14],po.U1[47:48],
                 po.U1[15],po.U1[49:50],po.U1[16],po.U1[51:52],
                 po.U1[17],po.U1[53:54],po.U1[18],po.U1[55:56],
                 po.U1[19],po.U1[57:58],po.U1[20],po.U1[59:N])
  
  # construir a matriz contento os dados completos para a amostra de Polya
  po.dados <- cbind(dadosPop[ind.f1,1],
                    dadosPop[ind.f2,2],
                    dadosPop[ind.f3,3],
                    dadosPop[ind.f4,4],
                    dadosPop[ind.U1,5],
                    dadosPop[ind.U1,6],
                    dadosPop[ind.U1,7],
                    dadosPop[ind.U1,8])
  
  # calcular todas as estatísticas de interesse para as quais se deseja
  # as inferencias populacionais
  for(j in 1:N){
    Esf[j] <- sum(po.dados[j,1:4])
    Y[j, ] <- Esf[j]*po.dados[j,5:8]
    UT[j] <- sum(Y[j,])
  }
  Ty.est[i] <- sum(UT)
  Ty1.est[i] <- sum(Y[,1])
  Ty2.est[i] <- sum(Y[,2])
  Ty3.est[i] <- sum(Y[,3])
  Ty4.est[i] <- sum(Y[,4])
  Tf.est[i] <- sum(Esf)
}  ### FIM do LOOPING DE POLYA

#(d.) Produzir os sumários da posterior de Polya para cada uma das 
# funcoes populacionais de interesse
# Iniciar recuperando os parametros populacionais
load(file="CensoVirtual.Rda")
#sumarios censitarios
totais <- apply(censo,2,sum)
EsfTotal <- sum(totais[1:4])
U1Total <- sum(totais[c(5,9,13,17)])
U2Total <- sum(totais[c(6,10,14,18)])
U3Total <- sum(totais[c(7,11,15,19)])
U4Total <- sum(totais[c(8,12,16,20)])
UGTotal <- sum(U1Total+U2Total+U3Total+U4Total)
c(EsfTotal=EsfTotal,
  U1Total=U1Total,
  U2Total=U2Total, 
  U3Total=U3Total,
  U4Total=U4Total,
  UGTotal=UGTotal)



# d.1) CAPTURA TOTAL  Valores em Toneladas kg/1000.
hist(Ty.est/1000,nclass=30, main = "Ty Captura Total (ton)")
abline(v=mean(Ty.est)/1000,col="red",lwd=2)
abline(v = quantile(Ty.est/1000, probs = c(0.025, 0.975)),
       col = "red",lty=21)
mean(Ty.est/1000)
#coeficient of variation of the Polya posterior
sd(Ty.est)/mean(Ty.est)  
# intervalo (quantil) de credibilidade 
quantile(Ty.est/1000, probs = c(0.025, 0.975))
# intervalo HDI de credibilidade
hdi(Ty.est/1000, 0.95)
abline(v = hdi(Ty.est/1000,0.95),col = "blue",lty=21)
abline(v=UGTotal/1000,lwd=2,col="green")

# d.2) CAPTURA de Camaroes (U3)  Valores em toneladas kg/1000
hist(Ty3.est/1000,nclass=30, main = "Ty3 Captura Camaroes (ton)")
abline(v=mean(Ty3.est/1000),col="red",lwd=2)
abline(v = quantile(Ty3.est/1000, probs = c(0.025, 0.975)),
       col = "red",lty=21)
mean(Ty3.est/1000)
#coeficient of variation of the Polya posterior
sd(Ty3.est)/mean(Ty3.est)  
# intervalo quantil de credibilidade
quantile(Ty3.est/1000, probs = c(0.025, 0.975))
# intervalo HDI de credibilidade
hdi(Ty3.est/1000, 0.95)
abline(v = hdi(Ty3.est/1000,0.95),col = "blue",lty=21)
abline(v=U3Total/1000,lwd=2,col="green")

# d.3) CAPTURA de Camaroes (U1)  Valores em toneladas kg/1000
hist(Ty1.est/1000,nclass=30, main = "Ty1 (ton)")
abline(v=mean(Ty1.est/1000),col="red",lwd=2)
abline(v = quantile(Ty1.est/1000, probs = c(0.025, 0.975)),
       col = "red",lty=21)
mean(Ty1.est/1000)
#coeficient of variation of the Polya posterior
sd(Ty1.est)/mean(Ty1.est)  
# intervalo quantil de credibilidade
quantile(Ty1.est/1000, probs = c(0.025, 0.975))
# intervalo HDI de credibilidade
hdi(Ty1.est/1000, 0.95)
abline(v = hdi(Ty1.est/1000,0.95),col = "blue",lty=21)
abline(v=U1Total/1000,lwd=2,col="green")

# d.4) CAPTURA de (U2)  Valores em toneladas kg/1000
hist(Ty2.est/1000,nclass=30, main = "Ty2 (ton)")
abline(v=mean(Ty2.est/1000),col="red",lwd=2)
abline(v = quantile(Ty2.est/1000, probs = c(0.025, 0.975)),
       col = "red",lty=21)
mean(Ty2.est/1000)
#coeficient of variation of the Polya posterior
sd(Ty2.est)/mean(Ty2.est)  
# intervalo quantil de credibilidade
quantile(Ty2.est/1000, probs = c(0.025, 0.975))
# intervalo HDI de credibilidade
hdi(Ty2.est/1000, 0.95)
abline(v = hdi(Ty2.est/1000,0.95),col = "blue",lty=21)
abline(v=U2Total/1000,lwd=2,col="green")

# d.5) CAPTURA de (U4)  Valores em toneladas kg/1000
hist(Ty4.est/1000,nclass=30, main = "Ty4 (ton)")
abline(v=mean(Ty4.est/1000),col="red",lwd=2)
abline(v = quantile(Ty4.est/1000, probs = c(0.025, 0.975)),
       col = "red",lty=21)
mean(Ty4.est/1000)
#coeficient of variation of the Polya posterior
sd(Ty4.est)/mean(Ty4.est)  
# intervalo quantil de credibilidade
quantile(Ty4.est/1000, probs = c(0.025, 0.975))
# intervalo HDI de credibilidade
hdi(Ty4.est/1000, 0.95)
abline(v = hdi(Ty4.est/1000,0.95),col = "blue",lty=21)
abline(v=U4Total/1000,lwd=2,col="green")

# d.) Esforco Total (viagens)
hist(Tf.est,nclass=30, main = "Tf (viagens)")
abline(v=mean(Tf.est),col="red",lwd=2)
abline(v = quantile(Tf.est, probs = c(0.025, 0.975)),col = "red",lty=21)
mean(Tf.est)
#coeficient of variation of the Polya posterior
sd(Tf.est)/mean(Tf.est)  
# intervalo quantil de credibilidade
quantile(Tf.est, probs = c(0.025, 0.975))
# intervalo HDI de credibilidade
hdi(Tf.est, 0.95)
abline(v = hdi(Tf.est,0.95),col = "blue",lty=21)
abline(v=EsfTotal,lwd=2,col="green")
